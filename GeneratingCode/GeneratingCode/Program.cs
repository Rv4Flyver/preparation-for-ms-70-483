﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GeneratingCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(200, 70);
            Console.SetWindowPosition(0, 0);

            CodeDOMExample();
            Console.ReadKey();
            Console.Clear();

            ExpressionTreeExample();
            Console.ReadKey();
        }

        static void CodeDOMExample()
        {
            // We can call it a class that represents the file containing some code
            CodeCompileUnit unit = new CodeCompileUnit();

            // Declaring a namespace
            CodeNamespace nameSpace = new CodeNamespace("GeneratedCode");
            // Adding references to other NAMESPACES 
            nameSpace.Imports.Add(new CodeNamespaceImport("System"));
            unit.Namespaces.Add(nameSpace);

            // Declaring PUBLIC CLASS
            CodeTypeDeclaration clss = new CodeTypeDeclaration("GeneratedClass");
            clss.IsClass = true;
            clss.TypeAttributes = TypeAttributes.Public;
            nameSpace.Types.Add(clss);


            // C O D E M E M B E R S
            // Adding FIELDS
            CodeMemberField field = new CodeMemberField(typeof(double), "field1");
            field.Attributes = MemberAttributes.Private;
            clss.Members.Add(field);

            field = new CodeMemberField(typeof(double), "field2");
            clss.Members.Add(field);

            // Adding PROPERTIES
            CodeMemberProperty property = new CodeMemberProperty();
            property.Attributes = MemberAttributes.Public | MemberAttributes.Final;
            property.Type = new CodeTypeReference(typeof(double));
            property.Name = "Field1";
            property.HasGet = property.HasSet = true;
            property.GetStatements.Add(
                new CodeMethodReturnStatement(
                    new CodeFieldReferenceExpression(
                        new CodeThisReferenceExpression(), "field"
                        )
                    )
                );
            property.SetStatements.Add(
                new CodeAssignStatement(
                    new CodeFieldReferenceExpression(
                        new CodeThisReferenceExpression(), "field"
                        ), new CodePropertySetValueReferenceExpression()
                    )
                );

            clss.Members.Add(property);

            // Adding Methods
            CodeMemberMethod method = new CodeMemberMethod();
            method.Attributes = MemberAttributes.Public;
            method.Name = "Method";
            
            #region IF statement
            CodeConditionStatement ifLogic = new CodeConditionStatement();
            ifLogic.Condition = new CodeBinaryOperatorExpression(
                    new CodeFieldReferenceExpression(
                        new CodeThisReferenceExpression(), property.Name
                    ),
                    CodeBinaryOperatorType.ValueEquality,
                    new CodePrimitiveExpression(0)
                );
            ifLogic.TrueStatements.Add(
                    new CodeMethodReturnStatement(new CodePrimitiveExpression(0))
                );
            ifLogic.FalseStatements.Add(
                    new CodeMethodReturnStatement(new CodePrimitiveExpression(1))
                );

            method.Statements.Add(ifLogic);
            #endregion

            #region FOR LOOP statement
            CodeIterationStatement forLoop = new CodeIterationStatement();
            var num = new CodeVariableDeclarationStatement(typeof(int), "num", new CodePrimitiveExpression(0));
            forLoop.InitStatement = new CodeAssignStatement(new CodeVariableReferenceExpression("num"), new CodePrimitiveExpression(1));
            forLoop.TestExpression = new CodeBinaryOperatorExpression(
                                            new CodeVariableReferenceExpression("num"), 
                                            CodeBinaryOperatorType.LessThan, 
                                            new CodePrimitiveExpression(10) 
                                     );
            forLoop.IncrementStatement = new CodeAssignStatement(
                                            new CodeVariableReferenceExpression("num"),
                                                new CodeBinaryOperatorExpression(
                                                    new CodeVariableReferenceExpression("num"),
                                                    CodeBinaryOperatorType.Add,
                                                    new CodePrimitiveExpression(1)
                                                )
                                            );
            var forLoopStatement = new CodeExpressionStatement(
                                        new CodeMethodInvokeExpression(
                                            new CodeMethodReferenceExpression(
                                                new CodeTypeReferenceExpression("Console"),
                                                "WriteLine"
                                            ),   
                                            new CodeMethodInvokeExpression(
                                                new CodeVariableReferenceExpression("num"),
                                                "ToString"
                                            )
                                        )
                                    );
            forLoop.Statements.Add(forLoopStatement);
            method.Statements.Add(forLoop);
            #endregion

            #region WHILE loop
            CodeIterationStatement whileLoop = new CodeIterationStatement();
            var n = new CodeVariableDeclarationStatement(typeof(int), "n", new CodePrimitiveExpression(0));
            whileLoop.IncrementStatement = whileLoop.InitStatement = new CodeSnippetStatement(); // WTF?! Why CodeStatement is not supported?! Bastards!1
            whileLoop.TestExpression = new CodeBinaryOperatorExpression(
                                            new CodeVariableReferenceExpression("n"),
                                            CodeBinaryOperatorType.LessThan,
                                            new CodePrimitiveExpression(10)
                                     );

            var whileLoopStatement = new CodeExpressionStatement(
                                        new CodeMethodInvokeExpression(
                                            new CodeMethodReferenceExpression(
                                                new CodeTypeReferenceExpression("Console"),
                                                "WriteLine"
                                            ),
                                            new CodeMethodInvokeExpression(
                                                new CodeVariableReferenceExpression("n"),
                                                "ToString"
                                            )
                                        )
                                    );
            whileLoop.Statements.Add(whileLoopStatement);

            var whileIncrement = new CodeAssignStatement(
                    new CodeVariableReferenceExpression("n"),
                    new CodeBinaryOperatorExpression(
                        new CodeVariableReferenceExpression("n"),
                        CodeBinaryOperatorType.Add,
                        new CodePrimitiveExpression(1)
                    )
                );
            whileLoop.Statements.Add(whileIncrement);
            method.Statements.Add(whileLoop);
            #endregion

            clss.Members.Add(method);

            // CodeDOMProvider
            CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
            CodeGeneratorOptions options = new CodeGeneratorOptions();
            options.BlankLinesBetweenMembers = false;
            options.BracingStyle = "C";
            //using (StreamWriter sourceWriter = new StreamWriter("chsarp.cs"))
            //{
            //    provider.GenerateCodeFromCompileUnit(unit, sourceWriter, options);
            //}
            using (StringWriter sourceWriter = new StringWriter())
            {
                provider.GenerateCodeFromCompileUnit(unit, sourceWriter, options);
                Console.WriteLine(sourceWriter.ToString());
            }
        }

        static void ExpressionTreeExample()
        {
            // Expression trees are immutable, which means that they cannot be modified directly. 
            // To change an expression tree, you must create a copy of an existing expression tree and when you create the copy, make the required changes. 
            // You can use the ExpressionVisitor class to traverse an existing expression tree and to copy each node that it visits.

            // Only expression trees that represent lambda expressions can be executed. 
            // Expression trees that represent lambda expressions are of type LambdaExpression or Expression<TDelegate>. 
            // To execute these expression trees, call the Compile method to create an executable delegate, and then invoke the delegate.

            // If an expression tree does not represent a lambda expression, you can create a new lambda expression that has the original 
            // expression tree as its body, by calling the Lambda<TDelegate>(Expression, IEnumerable<ParameterExpression>) method. 
            // Then, you can execute the lambda expression as described earlier in this section.

            //1
            Expression<Func<int>> expression = Expression.Lambda<Func<int>>(Expression.Constant(100500));
            Console.WriteLine(expression.Compile()());
            Console.WriteLine("Expression parameters count: " + expression.Parameters.Count());
            ConstantExpression operation = (ConstantExpression)expression.Body;
            Console.WriteLine(operation.CanReduce);
            
            //2
            BlockExpression blockExpr = Expression.Block(
                 Expression.Call(
                     null,
                     typeof(Console).GetMethod("Write", new Type[] { typeof(String) }),
                     Expression.Constant("Hello ")
                 ),
                 Expression.Call(
                     null,
                     typeof(Console).GetMethod("WriteLine", new Type[] { typeof(String) }),
                     Expression.Constant("World!")
                 )
            );
            Expression.Lambda<Action>(blockExpr).Compile()();

            //3
            ParameterExpression value = Expression.Parameter(typeof(int), "value");
            // Creating an expression to hold a local variable. 
            ParameterExpression result = Expression.Parameter(typeof(int), "result");
            LabelTarget label = Expression.Label(typeof(int));
            Expression loop = Expression.Loop(
                                               // Adding a conditional block into the loop.
                                               Expression.IfThenElse(
                                                    // Condition: value > 1
                                                   Expression.GreaterThan(value, Expression.Constant(1)),
                                                    // If true: result *= value --
                                                   Expression.MultiplyAssign(result,
                                                       Expression.PostDecrementAssign(value)),
                                                    // If false, exit the loop and go to the label.
                                                   Expression.Break(label, result)
                                               ),
                                                // Label to jump to.
                                                   label
                                                );
            // Assigning a constant to a local variable: result = 1
            Expression localVariable =  Expression.Assign(result, Expression.Constant(1));
            BlockExpression block = Expression.Block(new[] { result }, localVariable, loop);

            Console.WriteLine(Expression.Lambda<Func<int, int>>(block, value).Compile()(5));

        }
    }
}
