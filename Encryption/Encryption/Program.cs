﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Encryption
{
    class Program
    {
        static void Main(string[] args)
        {

            Symmetric();

            Console.Clear();

            Asymmetric();

            Console.Clear();

            Console.WriteLine(Encoding.Default.GetString(ProtectString("Waaagh!")));

            Console.ReadKey();

            Console.Clear();

            StreamEncryptionDecryption();

            Console.ReadKey();

            Console.Clear();

            HashAlgorithm sha = SHA256.Create();
            byte[] hashData = sha.ComputeHash(Encoding.Default.GetBytes("Waaagh!"));
            Console.WriteLine(Convert.ToBase64String(hashData));
            
            Console.ReadKey();

            Console.Clear();
        }

        static void Symmetric()
        {
            string text = "Rvach.blogspot.com";
            byte[] plainData, cipher, key, IV;
            SymmetricAlgorithm cryptoAlgoritm;
            ICryptoTransform cryptor;

            #region Encryptor

                plainData = Encoding.ASCII.GetBytes(text);

                // This static method creates a new cryptographic using the default 
                // algorithm, which in .NET 4.5 is RijndaelManaged.
                cryptoAlgoritm = SymmetricAlgorithm.Create();
                cryptor = cryptoAlgoritm.CreateEncryptor(cryptoAlgoritm.Key, cryptoAlgoritm.IV);
                cipher = cryptor.TransformFinalBlock(plainData, 0, plainData.Length);

                key = cryptoAlgoritm.Key;
                IV = cryptoAlgoritm.IV;

                cryptoAlgoritm.Clear();

                Console.WriteLine("Cipher: " + ASCIIEncoding.ASCII.GetString(cipher));

            #endregion

            #region Decryptor

                cryptoAlgoritm = SymmetricAlgorithm.Create();
                cryptor = cryptoAlgoritm.CreateDecryptor(key, IV);
                plainData = cryptor.TransformFinalBlock(cipher, 0, cipher.Length);

                cryptoAlgoritm.Clear();

                Console.WriteLine("Plain: " + ASCIIEncoding.ASCII.GetString(plainData));

            #endregion

           Console.ReadKey();
        }

        static void Asymmetric()
        {
            string publicKeyXML, privateKeyXML;
            byte[] dataToEncrypt, encryptedData, decryptedData;

            // Exporting public and then private part of then key to XML
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            publicKeyXML = rsa.ToXmlString(false);
            privateKeyXML = rsa.ToXmlString(true);

            // Using a public and private key to encrypt and decrypt data
            dataToEncrypt = Encoding.ASCII.GetBytes("My Secret Data!");

            #region Encrypt

                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSA.FromXmlString(publicKeyXML);
                    encryptedData = RSA.Encrypt(dataToEncrypt, false);
                }
                Console.WriteLine("Encrypted Data: " + Encoding.ASCII.GetString(encryptedData));

            #endregion

            #region Decrypt

                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSA.FromXmlString(privateKeyXML);
                    decryptedData = RSA.Decrypt(encryptedData, false);
                }

                string decryptedString = Encoding.ASCII.GetString(decryptedData);
                Console.WriteLine("Decrypted Data: " + decryptedString); // Displays: My Secret Data!

            #endregion

            Console.ReadKey();
        }

        static byte[] ProtectString(string data)
        {
            byte[] userData = System.Text.Encoding.Default.GetBytes(data);
            byte[] encryptedData = ProtectedData.Protect(userData, null, DataProtectionScope.CurrentUser);
            return encryptedData;
        }

        static void StreamEncryptionDecryption()
        {
            string text = "Rvach.blogspot.com";
            byte[] plainData, cipher, key, IV;
            SymmetricAlgorithm cryptoAlgoritm;
            ICryptoTransform cryptor;

            plainData = Encoding.ASCII.GetBytes(text);

            // This static method creates a new cryptographic using the default 
            // algorithm, which in .NET 4.5 is RijndaelManaged.
            cryptoAlgoritm = SymmetricAlgorithm.Create();
            cryptor = cryptoAlgoritm.CreateEncryptor(cryptoAlgoritm.Key, cryptoAlgoritm.IV);

            key = cryptoAlgoritm.Key;
            IV = cryptoAlgoritm.IV;

            Console.WriteLine("Encrypted:");
            using(MemoryStream memory = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(memory, cryptor, CryptoStreamMode.Write))
                {
                    plainData = Encoding.Default.GetBytes(text);
                    stream.Write(plainData, 0, plainData.Length);
                }
                cipher = memory.ToArray();
                Console.WriteLine(Encoding.Default.GetString(memory.ToArray()));
            }

            Console.WriteLine("Decrypted:");
            using (MemoryStream memory = new MemoryStream(cipher))
            {
                cryptor = cryptoAlgoritm.CreateDecryptor(key, IV);
                using (CryptoStream stream = new CryptoStream(memory, cryptor, CryptoStreamMode.Read))
                {
                    stream.Read(plainData,0,8);
                    Console.WriteLine(Encoding.Default.GetString(plainData));
                }
            }
        }

    }
}
