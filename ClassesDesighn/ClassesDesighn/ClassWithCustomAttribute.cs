﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesDesighn
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Assembly, AllowMultiple = true, Inherited = true)]
    class HelpAttribute : Attribute
    {
        public string field = "field";

        public string Help
        {
            get;
            set;
        }

        public HelpAttribute()
        {
            Help = "no help";
        }

        public HelpAttribute(string help)
	    {
            this.Help = help;
	    }
    }

    ///Positional parameters are parameters of the constructor of the attribute. 
    ///They are mandatory and a value must be passed every time the attribute is placed on any program entity. 
    ///On the other hand Named parameters are actually optional and are not parameters of the attribute's constructor.
    ///So Don't use multiple constructors for optional parameter. Instead, mark them as named parameters. 
    ///
    ///What happen behind the scene is first the constructor is called with positional parameters, and then set method is called for each named parameter. 
    ///The value set in the constructor is override by the set method.
    [Help]
    class ClassWithCustomAttribute
    {

        public ClassWithCustomAttribute()
        {
            Console.WriteLine("ClassWithCustomAttribute ctor");

            ReadAttributes();

            // Method invoked only when debugging
            MethodDebug();
        }

        [Obsolete("This method should not be used anymore!", true)]
        public void Method0()
        { 
            // do smth
        }

        [System.Diagnostics.Conditional("Debug")]
        public void MethodDebug()
        {
            Console.WriteLine("Method for debug!");
        }

        protected void ReadAttributes()
        {
            // if  AllowMultiple = false but inherited is true the base class Help is overridden by the Derive class Help attribute.
            Console.WriteLine("Assigned attributes: "+this.GetType().GetCustomAttributes(true).Count());
            HelpAttribute attr = (HelpAttribute)this.GetType().GetCustomAttributes(true).First();
            Console.WriteLine(attr.Help);
            Console.WriteLine(attr.field);
        }
    }

    [Help(field = "Some help field")]
    class DerivedClassWithCustomAttribute : ClassWithCustomAttribute
    {
        public DerivedClassWithCustomAttribute()
        {
            Console.WriteLine("DerivedClassWithCustomAttribute ctor");

            ReadAttributes();
        }

        public DerivedClassWithCustomAttribute(double d)
        {

        }
    }
}
