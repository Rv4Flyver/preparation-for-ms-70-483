﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ClassesDesighn
{
    class DynamicObjectExample : DynamicObject
    {
        Dictionary<string,object> dynamicMembers = new Dictionary<string,object>();

        /// <summary>
        /// C# и Visual Basic компиляторы никогда не выпускают код для использования этого метода, 
        /// поскольку они не поддерживают типы первого класса. Этот метод предназначен для языков, 
        /// поддерживающих инициализацию динамических объектов с помощью синтаксиса, подобного dynamic new.
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryCreateInstance(CreateInstanceBinder binder, object[] args, out object result)
        {
            Console.WriteLine("TryCreateInstance");
            result = null;
            return true;
        }

        /// <summary>
        /// Provides the implementation for operations that invoke an object or a delegate.
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryInvoke(InvokeBinder binder, object[] args, out object result)
        {
            Console.Write("TryInvoke was called with object[] args: ");
            foreach(object arg in args)
                Console.Write(arg+", ");

            Console.WriteLine();
            result = null;
            return true;
        }

        /// <summary>
        /// It specifies dynamic behavior for operations such as calling a method.
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="args"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            
            result = null;
            if (dynamicMembers.ContainsKey(binder.Name))
            {
                dynamic method = dynamicMembers[binder.Name];
                result = method(args[0]);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            if (dynamicMembers.ContainsKey(binder.Name))
            {
                result = dynamicMembers[binder.Name];
                return true;
            }
            else
            {
                result = "Does not contain " + binder.Name;
                return false;
            }
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            try
            {
                dynamicMembers[binder.Name] = value;
            }
            catch
            {
                return false;
            }
            return true;
        }

    }
}
