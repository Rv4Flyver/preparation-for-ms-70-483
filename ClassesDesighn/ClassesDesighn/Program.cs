﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Diagnostics;
using Microsoft.CSharp;
using System.IO;
using System.Collections.Generic;
using Microsoft.CSharp.RuntimeBinder;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.InteropServices;



namespace ClassesDesighn
{


    public delegate void ProgramMethodDelegate();

    class Program
    {
        #region
        const int SWP_NOSIZE = 0x0001;

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        private static IntPtr MyConsole = GetConsoleWindow();

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);
        #endregion

            public void Method(int number, string text, bool isTrue = true)
            {
                string localVariable = "localVariable";

                if (localVariable != String.Empty)
                {
                    Console.WriteLine();
                }
            }

        static void Main(string[] args)
        {
            #region Setting Window and Log parameters
            Console.SetWindowSize(206, 81);
            SetWindowPos(MyConsole, 0, 0, 0, 0, 0, SWP_NOSIZE);

            #endregion

            DerivedClass dc = new DerivedClass();
            //dc.Field = "field property";
            Console.WriteLine(dc.Field);
            // Property or indexer 'ClassesDesighn.IClass.Field' cannot be assigned to -- it is read only
            //((IClass)dc).Field = "field property"; 

            #region Reflection
            Console.ReadKey();
            Console.Clear();

            MethodInfo[] methodsInfos;

            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nPublic methods of " + dc.GetType().Name + ":");
            Console.ResetColor();
            //Reflect instace public methods
            methodsInfos = dc.GetType().GetMethods((BindingFlags.Instance | BindingFlags.Public));
            // or in particular case shorter variant is:
            // methodsInfos = dc.GetType().GetMethods();
            ReflectionExample(methodsInfos, dc);

            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Private methods of " + dc.GetType().Name + ":");
            Console.ResetColor();
            //Reflect instace pivate methods
            methodsInfos = dc.GetType().GetMethods((BindingFlags.Instance | BindingFlags.NonPublic));
            ReflectionExample(methodsInfos, dc);
            Console.WriteLine();



            Console.WriteLine();
            BaseClass bc = new BaseClass();
            // Gets the members (properties, methods, fields, events, and so on) of the current Type.
            MemberInfo[] mInfos = bc.GetType().GetMembers();
            Console.WriteLine("Membersinfo");
            foreach (MemberInfo info in mInfos)
            {
                Console.WriteLine("type  "+info.MemberType+", name "+info.Name);
            }
            Console.WriteLine();

            Console.WriteLine();
            // Gets the members (properties, methods, fields, events, and so on) of the current Type.
            PropertyInfo[] pInfos = bc.GetType().GetProperties();
            Console.WriteLine("PropertiesInfo");
            foreach (PropertyInfo info in pInfos)
            {
                Console.WriteLine("type " + info.PropertyType + ", name " + info.Name + ", value " + info.GetValue(bc));
            }
            Console.WriteLine();

            Console.WriteLine();
            Module[] modules = Assembly.GetExecutingAssembly().GetModules();
            Console.WriteLine("Modules");
            foreach (Module module in modules)
            {
                Console.WriteLine("Name: {0} \nScopeName: {1} \nTypes: ",module.Name, module.ScopeName);
                foreach (Type type in module.GetTypes())
                {
                    Console.Write(" " + type.Name);
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            Console.WriteLine(dc.GetType().IsSubclassOf(typeof(BaseClass)) + "\n");


            Console.WriteLine();
            Console.WriteLine("EventInfo");
            EventInfo eInfo =  bc.GetType().GetEvent("FieldChanged");
            Console.WriteLine(eInfo.AddMethod.Name);
            Console.WriteLine(eInfo.RemoveMethod.Name);
            Console.WriteLine(eInfo.DeclaringType);
            Console.WriteLine(eInfo.EventHandlerType);
            Console.WriteLine(eInfo.IsMulticast);
            Console.WriteLine(eInfo.IsSpecialName);
            Console.WriteLine(eInfo.MemberType);
            Console.WriteLine(eInfo.MetadataToken);
            Console.WriteLine(eInfo.RaiseMethod);
            //NULL : This is because the C# and Visual Basic compilers do not generate such a method by default.
            Console.WriteLine(eInfo.ReflectedType);
            Console.WriteLine();
            Console.WriteLine("Other methods:" + eInfo.GetOtherMethods().Length);
            ProgramMethodDelegate pdelg = new ProgramMethodDelegate(delegate { Console.WriteLine("new delegate"); });
            DelegateMethod delg = new DelegateMethod(pdelg);
            eInfo.AddEventHandler(bc, delg);
            bc.OnChanged();
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("FieldInfo");
            FieldInfo fInfo = bc.GetType().GetField("field");
            Console.WriteLine(fInfo.DeclaringType);
            Console.WriteLine(fInfo.FieldHandle.Value);
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("LocalVariableInfo");
            MethodInfo mInfo = typeof(Program).GetMethod("Method");
            MethodBody mb = mInfo.GetMethodBody();
            foreach (LocalVariableInfo info in mb.LocalVariables)
            {
                Console.WriteLine("Index: {2}, Local type: {0}, IsPinned: {1}, ", info.LocalType, info.IsPinned, info.LocalIndex);
            }
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("ParameterInfo");
            Console.WriteLine("Position | Name   \t| Is In   \t| IsOut   \t| IsOptional  \t| ParameterType \t| Member \t\t\t\t\t| RawDefaultValue");
            foreach (ParameterInfo info in mInfo.GetParameters())
            {
                Console.WriteLine("{0} \t | {1}  \t| {2}  \t| {3}  \t| {4}  \t| {5}  \t| {6}  \t| {7} ", info.Position, info.Name, info.IsIn, info.IsOut, info.IsOptional, info.ParameterType, info.Member, info.RawDefaultValue);
            }
            Console.WriteLine();

            Assembly asm = Assembly.GetExecutingAssembly();

            foreach (var atr in asm.GetCustomAttributes())
            {
                Console.WriteLine(atr.ToString());
            }

            #endregion

            #region Reflection - Attributes
            Console.ReadKey();
            Console.Clear();

            ClassWithCustomAttribute classWithCustomAttribute = new ClassWithCustomAttribute();

            // cause desighn-time error
            // classWithCustomAttribute.Method0();
            Console.WriteLine();

            DerivedClassWithCustomAttribute derivedClassWithCustomAttribute = new DerivedClassWithCustomAttribute();

            Console.WriteLine();

            // Query Attributes in an assembly
            HelpAttribute HelpAttr;
            String assemblyName;
            Process p = Process.GetCurrentProcess();
            assemblyName = p.ProcessName + ".exe";
            //Assembly a = Assembly.LoadFrom(assemblyName);
            Assembly a = Assembly.GetExecutingAssembly();
            foreach (Attribute attr in a.GetCustomAttributes(true))
            {
                HelpAttr = attr as HelpAttribute;
                if (HelpAttr != null)
                {
                    Console.WriteLine("Description of {0}:\n{1}",
                                      assemblyName, HelpAttr.Help);
                }
            }

            //Querying Class Attributes
            foreach (Attribute attr in typeof(ClassWithCustomAttribute).GetCustomAttributes(true))
            {
                HelpAttr = attr as HelpAttribute;
                if (null != HelpAttr)
                {
                    Console.WriteLine("Description of ClassWithCustomAttribute:\n{0}",
                                      HelpAttr.Help);
                }
            }
            #endregion

            #region Dynamic Objects
            Console.ReadKey();
            Console.Clear();

            // DynamicObject
            dynamic obj = new DynamicObjectExample();
            try
            {
                obj(2,1,7,8);
                obj.Name = dc;
                Console.WriteLine(obj.Name);         // Displays ‘DerivedClass’
                Console.WriteLine(obj.SomeProperty); // Exception will be arised 
            }
            catch (RuntimeBinderException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // ExpandoObject
            ExpandoObjectExample();

            
            #endregion

            #region CodeDOM
            CodeCompileUnit compileUnit = new CodeCompileUnit();
            CodeNamespace myNamespace= new CodeNamespace("MyNamespace");
            myNamespace.Imports.Add(new CodeNamespaceImport("System")); 
            CodeTypeDeclaration myClass = new CodeTypeDeclaration("MyClass");
            CodeEntryPointMethod start = new CodeEntryPointMethod();
            CodeMethodInvokeExpression cs1 = new CodeMethodInvokeExpression(
                                                                             new CodeTypeReferenceExpression("Console"),
                                                                             "WriteLine", 
                                                                             new CodePrimitiveExpression("Hello World!")
                                                                           );
            compileUnit.Namespaces.Add(myNamespace);
            myNamespace.Types.Add(myClass);
            myClass.Members.Add(start);
            start.Statements.Add(cs1);


            CodeGeneratorOptions codeGeneratorOptions = new CodeGeneratorOptions();
            CSharpCodeProvider codeProvider = new CSharpCodeProvider();
            ICodeGenerator codeGenerator = codeProvider.CreateGenerator();

            Stream file = File.OpenWrite("generatedCode.txt");
            StreamWriter writer = new StreamWriter(file);

            codeGenerator.GenerateCodeFromCompileUnit(compileUnit, writer, codeGeneratorOptions);
            writer.Close();
            file.Close();

            #endregion



            Console.ReadKey();
            Console.Clear();
            GC.Collect();

            // Wait for all finalizers to complete before continuing. 
            // Without this call to GC.WaitForPendingFinalizers,  
            // the worker loop below might execute at the same time  
            // as the finalizers. 
            // With this call, the worker loop executes only after 
            // all finalizers have been called.
            GC.WaitForPendingFinalizers();

        }

        /// <summary>
        /// So how does this all work? How does the ExpandoObject know to intercept method 
        /// calls and define properties? Well, as I mentioned above, the ExpandoObject is 
        /// a DynamicObject (it’s actually an IDynamicMetaObjectProvider, but DynamicObject 
        /// implements IDynamicMetaObjectProvider). There are many overridable methods in 
        /// DynamicObject but for our purposes, the three most important are: 
        /// TryGetmember(,,.), TrySetMember(…), and TryInvokeMember(…). 
        /// When implemented in a subclass, the DLR will call the appropriate method during 
        /// runtime. So, for instance, when a property is set on an instance of ExpandoObject 
        /// the DLR calls TrySetMember (with some parameters) on ExpandoObject then 
        /// ExpandoObject does stuff and returns either true or false; true if the call was 
        /// successful (which it always is), false otherwise. If false is returned, you will 
        /// get the error above (object does not contain a definition for blah blah blah). 
        /// </summary>
        static void ExpandoObjectExample()
        {
            dynamic expando = new System.Dynamic.ExpandoObject();

            /// properties
            expando.PropertyName = "name";
            expando.PropertySurname = "surname";
            Console.WriteLine(expando.PropertyName + " " + expando.PropertySurname);

            /// methods
            expando.print2console = (Action<string>)((message) => Console.WriteLine(message));
            expando.print2console("expando.pring2console was invoked!");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodsInfos"></param>
        /// <param name="dc"></param>
        static void ReflectionExample(MethodInfo[] methodsInfos, object dc)
        {
            Console.WriteLine("______________________________________________________________________________________________________________________________________________________________________________________________________________");


            foreach (MethodInfo mf in methodsInfos)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(mf.Name);
                Console.ResetColor();

                Console.Write("(");
                ParameterInfo[] paramsInfos = mf.GetParameters();
                foreach (ParameterInfo paramInfo in paramsInfos)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(paramInfo.ParameterType.Name + " ");
                    Console.ResetColor();

                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.Write(paramInfo.Name);
                    Console.ResetColor();
                }
                Console.WriteLine(")");
                if (mf.DeclaringType == typeof(DerivedClass) && paramsInfos.Length == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("# ");
                    mf.Invoke(dc, new object[] { });
                    Console.ResetColor();
                }
                Console.WriteLine();
            }

            Console.WriteLine("______________________________________________________________________________________________________________________________________________________________________________________________________________");

        }

    }

}
