﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesDesighn
{
    interface IClass
    {
        string Field { get; }
    }

    public delegate void DelegateMethod();

    public class BaseClass : IClass
    {
        public string field;
        public event DelegateMethod fieldChanged;

        public event DelegateMethod FieldChanged
        {
            add { 
                fieldChanged += value;
            }
            remove
            {
                fieldChanged -= value;               
            }
        }

        public string Field
        {
            get;
            protected set;
        }

        public BaseClass()
        {
            // Do not call virtual members from an object inside its constructor.
            // Derived classes can override them so their implementation can be  
            // complitely different from the Base's one.
            Field = "!@$$";
            SayHello();
        }
        /// <summary>
        /// Finalizer
        /// </summary>
        ~BaseClass()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("GoodBye from BaseClass!");
            Console.ResetColor();
        }

        public virtual void SayHello()
        {
            Console.WriteLine("Hello from BaseClass!");

            Field = "Hello from BaseClass!";
            FieldChanged += delegate { Console.WriteLine("Field value was changed to " + Field + "!"); };
            FieldChanged += Method1;
            //FieldChanged -= Method1;

        }

        protected void Method1()
        {
            Console.WriteLine("Method 1 was invoked in BaseClass");

        }

        protected void Method2()
        {
            Console.WriteLine("Method 2 was invoked in BaseClass");
        }

        public void OnChanged()
        {
            fieldChanged();       
        }
    }

    public class DerivedClass : BaseClass, IDisposable
    {
        bool isDisposed = false;
        
        public DerivedClass()
        {
            SayHello();
            base.Field = "field property";
        }
        /// <summary>
        /// Finalizer
        /// </summary>
        ~DerivedClass()
        {
            Dispose(false);
        }

        public override void SayHello()
        {
            Console.WriteLine("Hello from DerivedClass!");
        }

        protected new void Method1()
        {
            base.Method1();
            Console.WriteLine("Method 1 was invoked in DerivedClass");

        }

        protected new void Method2()
        {
            base.Method2();
            Console.WriteLine("Method 2 was invoked in DerivedClass");
        }


        public void Dispose()
        {
            Dispose(true);
        }

        public void Dispose(bool disposing)
        {
            // if object has not been already disposed
            if (!isDisposed && disposing)
            {
                // do dispose
                isDisposed = true;
            }
            else
            { 
                // if an object which should be disposed implements its own finalizer 
                // it is not neccessary to do anything there this object, so we pass 
                // false to Dispose(bool disposing) in ~DerivedClass()
                Console.WriteLine("Finalizer calls Dispose(false)");
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("GoodBye from DerivedClass!");
            Console.ResetColor();
        }
    }
}
