﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates_and_Callbacks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("D E L E G A T E S\n");
            DelegatesExample del = new DelegatesExample();
            Console.ReadKey();

            Console.WriteLine("\nL A M B D A S\n");
            LambdaExample lambda = new LambdaExample();
            Console.ReadKey();

            Console.WriteLine("\nE V E N T S\n");
            EventsExample events = new EventsExample(100500);
            events.OnEnd += (sender, e) => Console.WriteLine("\r" + DateTime.Now.ToLongTimeString() + " | Counting from {0} has been finished.", e.Value);
            events.OnEnd += (sender, e) => { throw new Exception(); };
            events.OnEnd += (sender, e) => Console.WriteLine(DateTime.Now.ToLongTimeString() +  " | Last subscriber has been executed.");
            try
            {
                events.StartCounting();
            }
            catch (AggregateException ex)
            {
                foreach (Exception exception in ex.InnerExceptions)
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + exception.Message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + ex.Message);
            }
            finally
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | End of line.");
            }
            Console.ReadKey();
        }

    }
}
