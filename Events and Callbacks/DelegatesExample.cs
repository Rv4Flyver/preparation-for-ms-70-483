﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates_and_Callbacks
{
    /// <summary>
    /// In C#, delegates form the basic building blocks for events. A delegate is a type that defines a method signature.
    /// </summary>
    class DelegatesExample
    {
        public delegate int Calculate(int x, int y);
        public delegate Parent ParentDelegate();
        public delegate Parent ChildDelegate(ChildOfParent cop);

        public DelegatesExample()
        {
            /// Instantiating delegates is easy since C# 2.0 added the automatic creation of a new delegate
            /// when a method group is assigned to a delegate type. An instantiated delegate is an object; 
            /// you can pass it around and give it as an argument to other methods. 
            Calculate calc = Add;

            /// Combining delegates together is called multicasting. += and -= is used to add/remove another
            /// method to the invocation list of an existing delegate instance respectively.
            calc += Multuply;

            PrintInvocationList(calc);


            Console.WriteLine();

            /// When you assign a method to a delegate, the method signature does not have to match 
            /// the delegate exactly. This is called covariance and contravariance. 
            /// Covariance - method has a return type that is more derived than defined in the delegate.
            ParentDelegate parentDelegate = ReturnNewChildOfParent;
            PrintInvocationList(parentDelegate);

            Console.WriteLine();

            /// Contravariance - parameter types of a method are less derived than those in the delegate.   
            ChildDelegate childDelegate = ReturnNewParent;
            PrintInvocationList(childDelegate);
        }

        public int Add(int x, int y)
        {
            return x + y;
        }

        public int Multuply(int x, int y)
        {
            return x * y;
        }

        public Parent ReturnNewParent(Parent cop)
        {
            return cop;
        }

        public ChildOfParent ReturnNewChildOfParent()
        {
            return new ChildOfParent();
        }

        void PrintInvocationList(Delegate delegat)
        {
            Console.WriteLine(delegat.ToString());
            foreach (Delegate invocationMethod in delegat.GetInvocationList())
                // to find out how many methods a multicast delegate is going to call, you can use the code:
                Console.WriteLine(invocationMethod.Method);
        }
    }

    #region Covariance and Contravariance Example Types
    class Parent
    { 
    
    }

    class ChildOfParent : Parent
    { 
        
    }
    #endregion

}
