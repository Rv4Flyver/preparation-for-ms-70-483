﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates_and_Callbacks
{
    class EventsExample
    {
        int eventValue;

        private event EventHandler<NewEventsArgs> onEnd = delegate { };

        public event EventHandler<NewEventsArgs> OnEnd
        {
            add
            {
                lock (onEnd)
                {
                    onEnd += value;
                    onEnd += (sender, e) => Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Subscriber has been added.");
                }
            }
            remove
            {
                lock (onEnd)
                {
                    onEnd -= value;
                    onEnd += (sender, e) => Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Subscriber has been removed.");
                }
            }
        }

        public EventsExample(int endvalue)
        {
            eventValue = endvalue;
        }

        public void StartCounting()
        {
            for (int i = eventValue; i >= 0; i--)
                Console.Write("\r"+DateTime.Now.ToLongTimeString() + " | " + i);

            try
            {
                
                onEnd(this, new NewEventsArgs(eventValue));
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + ex.Message);
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Restarting...");
                RaiseEventManually();
            }
            
        }

        void RaiseEventManually()
        {
            var exceptions = new List<Exception>();
            foreach (Delegate handler in onEnd.GetInvocationList())
            {
                try
                {
                    handler.DynamicInvoke(this, new NewEventsArgs(eventValue));
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }
            if (exceptions.Any())
            {
                throw new AggregateException(exceptions);
            }            
        }
    }

    class NewEventsArgs : EventArgs
    {
        public NewEventsArgs(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
    }
}
