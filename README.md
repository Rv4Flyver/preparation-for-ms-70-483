# MS 70-483: Programming in C# #
There is a set of Visual Studio 2013 Projects created while preparing to MS 70-483 exam. In fact here is everything you should know about C#.

Feel free to use, learn and if you will consider it possible - contribute.

## Content ##
### Manage Program Flow ###
* [Program Flow](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/Program%20Flow)
* [Classes Desighn](ClassesDesighn)
* [Validation_and_Conversion](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/Validation_and_Conversion/?at=master)
* [Exceptions](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/Exceptions)
* [Events and Callbacks](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/Events%20and%20Callbacks)
* [PInvoke_COM](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/PInvoke_COM/?at=master)

### Create and Use Types ###
* [Generating Code/Generating Code](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/GeneratingCode/GeneratingCode/?at=master)
* [Enumerators/Comparors](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/EnumeratorsComparors)

### Debug applications and implement security ###
* [Debug applications](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/Debug%20n%20Performance)
* [Security](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/Encryption)

### Other ###
* [Net Work](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/NetWork)
* [Ildasm Test](https://bitbucket.org/Rvach_Flyver/preparation-for-ms-70-483/src/7b62a391bfa679f4f01989f41a5e24864c101083/IldasmTest/?at=master)