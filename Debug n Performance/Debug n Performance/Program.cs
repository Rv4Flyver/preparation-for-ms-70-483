﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Debug_n_Performance
{
    class Program
    {
        static void Main(string[] args)
        {
            #region DEBUG
            Console.WriteLine("D E B U G  C L A S S  E X A M P L E");
            //DEBUG (Debug class marked with [Conditional("Debug")] attribute)
            TraceListener listener = new DelimitedListTraceListener("debugListener.txt","DelimitedListTraceListener");
            Debug.Listeners.Add(listener);
            
            Debug.WriteLine("Starting application");
            Debug.Indent();
            Debug.IndentLevel = 2;
            Debug.IndentSize = 2;
            int i = 1 + 2;
            //Debug.Assert(i == 31);
            Debug.WriteLineIf(i == 3, "WriteLineIf condition is true");
            Debug.Print("Today: {0}", DateTime.Now);
            Debug.WriteLineIf(i > 0, "i is greater than 0");
            Debug.Unindent();
            Debug.WriteLine("Ending application");

            Debug.Flush();                  // сливает буфер слушателям
            Debug.Listeners.Remove("DelimitedListTraceListener");
            #endregion

            #region Trace
            Console.WriteLine("\nT R A C E  E X A M P L E");
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));
            Trace.AutoFlush = true;         // Gets or sets whether Flush should be called on the Listeners after every write.
            Trace.Indent();
            Trace.WriteLine("Entering Main");
            Console.WriteLine("Hello World!");
            Trace.WriteLine("Exiting Main");
            Trace.Unindent();
            //Trace
            //Trace.Assert(i == 31);
            #endregion

            // TraceSource was added in .NET 2.0 and should be used instead of the static Trace class.
            #region TraceSource
            Console.WriteLine("\nT R A C E  S O U R C E");
            TraceSource traceSource1 = new TraceSource("myCriticalTraceSource", SourceLevels.Critical);
            traceSource1.Listeners.Add(new TextWriterTraceListener(Console.Out));
            traceSource1.TraceInformation("Tracing critical events...");
            traceSource1.TraceEvent(TraceEventType.Critical, 0, "Critical trace");
            traceSource1.TraceData(TraceEventType.Critical, 1, new object[] { "a", "b", "c" });
            traceSource1.TraceData(TraceEventType.Information, 1, new object[] { "d", "f", "g" });
            traceSource1.Flush();
            traceSource1.Close();
            ///You can use several different options for the TraceEventType enum:
            // ■ Critical This is the most severe option. It should be used sparingly and only for very 
            // serious and irrecoverable errors.
            // ■ Error This enum member has a slightly lower priority than Critical, but it still indicates 
            // that something is wrong in the application. It should typically be used to flag a prob-
            // lem that has been handled or recovered from.
            // ■ Warning This value indicates something unusual has occurred that may be worth 
            // investigating further. For example, you notice that a certain operation suddenly takes 
            // longer to process than normal or you flag a warning that the server is getting low on 
            // memory.
            // ■ Information This value indicates that the process is executing correctly, but there is 
            // some interesting information to include in the tracing output file. It may be informa-
            // tion that a user has logged onto a system or that something has been added to the 
            // database.
            // ■ Verbose This is the loosest of all the severity related values in the enum. It should be 
            // used for information that is not indicating anything wrong with the application and is 
            // likely to appear in vast quantities. For example, when instrumenting all methods in a 
            // type to trace their beginning and ending, it is typical to use the verbose event type.
            // ■ Stop, Start, Suspend, Resume, Transfer These event types are not indications of 
            // severity, but mark the trace event as relating to the logical flow of the application. They 
            // are known as activity event types and mark a logical operation’s starting or stopping, 
            // or transferring control to another logical operation. 
            #endregion
            // TABLE  TraceListeners in the .NET Framework
            //      N A M E                        O U T P U T
            // ConsoleTraceListener        Standard output or error stream
            // DelimitedListTraceListener  TextWriter
            // EventLogTraceListener       EventLog
            // EventSchemaTraceListener    XML-encoded, schema-compliant log file
            // TextWriterTraceListener     TextWriter
            // XmlWriterTraceListener      XML-encoded data to a TextWriter or stream.
            Trace.TraceWarning("Trace.TraceWarning");

            #region XMLWriteTraceListener
            using (XmlWriterTraceListener XMLListener = new XmlWriterTraceListener("errorXML.log"))
            {
                XMLListener.TraceEvent(new TraceEventCache(), "Message from XMLWriterTraceListener using TraceEvent", TraceEventType.Error, 0);
                XMLListener.WriteLine("Message from XMLWriterTraceListener");
                XMLListener.Flush();
            }

            #endregion

            #region EventLog
            Console.WriteLine("\nW R I T I N G  T O  W I N  E V E N T  L O G");
            //These messages can then be viewed by the Windows Event Viewer.
            if (!EventLog.SourceExists("MyNewSource"))
            {
                EventLog.CreateEventSource("MyNewSource", "MyNewLog");
                //return;
            }
            EventLog myLog = new EventLog();
            myLog.Source = "MyNewSource";
            myLog.Log = "MyNewLog";
            myLog.WriteEntry("Log event!");


            // using EventLogTraceListener
            // create a trace listener for event log
            EventLogTraceListener traceListener = new EventLogTraceListener();
            // add the listener to the collection
            Trace.Listeners.Add(traceListener);
            Trace.WriteIf(0 < 1, "Test message from EventLogTraceListener");


            #endregion

            #region Read EventLog
            Console.WriteLine("\nR E A D  E V E N T  L O G");
            EventLog log = new EventLog("MyNewLog");
            Console.WriteLine("Total entries: " + log.Entries.Count);
            EventLogEntry lastEntry = log.Entries[log.Entries.Count - 1];
            Console.WriteLine("\tIndex: \t{0}\n\tSource: \t{1}\n\tType: \t{2}\n\tTime: \t{3}\n\tMessage: \t{4}", lastEntry.Index, lastEntry.Source, lastEntry.EntryType, lastEntry.TimeWritten, lastEntry.Message);

            #endregion

            // _______________________________________________________________________
            #region Profiling
            Console.WriteLine("\nP R O F I L I N G");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Thread.Sleep(1000);
            sw.Stop();
            Console.WriteLine("\tStopwatch | Elapsed: " + sw.Elapsed);
            Console.WriteLine("\tStopwatch | ElapsedMilliseconds: " + sw.ElapsedMilliseconds);
            Console.WriteLine("\tStopwatch | ElapsedTicks: " + sw.ElapsedTicks);
            sw.Restart();
            Thread.Sleep(1000);
            sw.Stop();
            Console.WriteLine("\tStopwatch | Elapsed: " + sw.Elapsed);
            Console.WriteLine("\tStopwatch | ElapsedMilliseconds: " + sw.ElapsedMilliseconds);
            Console.WriteLine("\tStopwatch | ElapsedTicks: " + sw.ElapsedTicks);
            sw.Reset();
            #endregion

            #region PERFORMANCE COUNTER
            Console.WriteLine("\nP E R F O R M A N C E  C O U N T E R  E X A M P L E \n I Realtek PCIe GBE Family Controller Monitor (Bytes Received/sec)");
            using (PerformanceCounter pc = new PerformanceCounter("Network Interface", "Bytes Received/sec", "Realtek PCIe GBE Family Controller"))
            { 
                    int n = 0;
                    while (n < 20) 
                     {
                        Console.WriteLine("\t"+pc.NextValue());
                        n++;
                        Thread.Sleep(1000);
                     }
            }

            Console.WriteLine(" II perfmon.exe");
            /// To check this out go to perfmon.exe:
            /// 1) choose system monitor
            /// 2) click add counter
            /// 3) find category and click add
            string categoryName = "Example Counter Category";
            if (!PerformanceCounterCategory.Exists(categoryName))
            {
                CounterCreationDataCollection collection = new CounterCreationDataCollection();

                collection.Add(new CounterCreationData  {
                                                            CounterName = "Counter № 1",
                                                            CounterType = PerformanceCounterType.NumberOfItems32
                                                        });
                collection.Add(new CounterCreationData  {
                                                            CounterName = "Counter № 2",
                                                            CounterType = PerformanceCounterType.AverageTimer32
                                                        });
                collection.Add(new CounterCreationData  {
                                                            CounterName = "AverageTimer32SampleBase",
                                                            CounterType = PerformanceCounterType.AverageBase
                                                        });
                PerformanceCounterCategory.Create(categoryName, "Example Counters", PerformanceCounterCategoryType.SingleInstance, collection);
            }

            string categoryName2 = "Example Counter Category2";
            if (!PerformanceCounterCategory.Exists(categoryName2))
            {
                CounterCreationDataCollection collection = new CounterCreationDataCollection();

                collection.Add(new CounterCreationData
                {
                    CounterName = "Counter № 1",
                    CounterType = PerformanceCounterType.SampleFraction
                });
                collection.Add(new CounterCreationData
                {
                    CounterName = "Counter № 2",
                    CounterType = PerformanceCounterType.RawBase
                });
                PerformanceCounterCategory.Create(categoryName2, "Example Counters", PerformanceCounterCategoryType.SingleInstance, collection);
            }
            /// Сразу за счетчиком типа AverageCount64, AverageTimer32, 
            /// CounterMultiTimer, CounterMultiTimerInverse, CounterMultiTimer100Ns, CounterMultiTimer100NsInverse, 
            /// RawFraction, SampleFraction или SampleCounter должен следовать любой из основных типов счетчиков: 
            /// AverageBase, MultiBase, RawBase или SampleBase. 
            

            // reading
            using(PerformanceCounter counter = new PerformanceCounter(categoryName2, "Counter № 1"))
            {
                counter.ReadOnly = false;
                Console.Write("\tEnter seconds for Performance Counter № 1 monitoring: ");
                
                int sec = 0, n = 0;
                bool isNumber = Int32.TryParse(Console.ReadLine(), out sec);
                Console.WriteLine();
                Console.Write("\t");
                while (isNumber && n<sec)
                {
                    counter.Increment();
                    Console.Write("  "+counter.RawValue);
                    Thread.Sleep(1000);
                    n++;
                }
            }


            #endregion

            Console.ReadKey();
        }
    }
}
