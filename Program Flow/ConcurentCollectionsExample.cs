﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Flow
{
    class ConcurentCollectionsExample
    {
        public ConcurentCollectionsExample()
        {
            Console.WindowHeight=71;

            PrintSubExampleTitle("BlockingCollection");
            BlockingCollection();

            PrintSubExampleTitle("ConcurrentBag");
            ConcurrentBag();

            PrintSubExampleTitle("ConcurrentStack");
            ConcurrentStack();

            PrintSubExampleTitle("ConcurrentQueue");
            ConcurrentQueue();

            PrintSubExampleTitle("ConcurentDictionary");
            ConcurentDictionary();

        }

        void PrintSubExampleTitle(string title)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n"+title);
            Console.ResetColor();
        }

        /// <summary>
        /// BlockingCollection is in reality a wrapper around other collection types. If you don’t give it 
        /// any specific instructions, it uses the ConcurrentQueue by default
        /// </summary>
        void BlockingCollection()
        {
            BlockingCollection<string> col = new BlockingCollection<string>();
            DateTime startTime;

            // CONSTANTLY MONITORING FOR ELEMENTS IN BLOCKINGCOLLECTION
            Task read = Task.Run(() =>
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task reading items from BlockingCollection runs");
                startTime = DateTime.Now;
                // while (true)
                // {
                //     Console.WriteLine(col.Take() + " was taken. The input was being monitored from " + startTime.ToLongTimeString() + " till " + (startTime = DateTime.Now).ToLongTimeString());
                //     Console.Write("Enter anything you want: ");
                // }
                /// To avoid using while it is possible to use the GetConsumingEnumerable() instead to get an IEnumerable that blocks until it find a new item.
                foreach (string v in col.GetConsumingEnumerable())
                {
                    Console.WriteLine(v + " was taken. The input was being monitored from " + startTime.ToLongTimeString() + " till " + (startTime = DateTime.Now).ToLongTimeString());
                    Console.Write("Enter anything you want: ");
                }
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task reading items from BlockingCollection stops");

            });
            Task write = Task.Run(() =>
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task monitoring input starts \nEnter anything you want: ");                
                while (true)
                {
                    string s = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(s))
                    {
                        col.CompleteAdding();                   // is used to signal to the BlockingCollection that no more items will be added. 
                        break;                                  // If other threads are waiting for new items, they won't be blocked any-more.
                    }
                    col.Add(s);
                }
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task monitoring input stops");
            });

            write.Wait();                                       // Main Thread awaits for input
        }

        /// <summary>
        /// A ConcurrentBag is just a bag of items. It enables duplicates and it has no particular order. 
        /// Important methods are Add, TryTake, and TryPeek.
        /// </summary>
        void ConcurrentBag()
        { 
            ConcurrentBag<int> bag = new ConcurrentBag<int>();
            Task task1 = Task.Run(() =>
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task adding items into ConcurrentBag runs");
                bag.Add(42);
                Thread.Sleep(1000);
                bag.Add(21);
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task adding items into ConcurrentBag stops");
            });
            Task.Run(() =>
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task reading items from ConcurrentBag runs");
                Thread.Sleep(500);
                if (bag.Count > 0)
                {
                    /// One thing to keep in mind is that the TryPeek(out result) method is not very useful in a multithreaded 
                    /// environment. It could be that another thread removes the item before you can access it.
                    // int result;
                    // bag.TryPeek(out result)
                    // Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task reading items from ConcurrentBag returns " + result);      

                    /// ConcurrentBag also implements IEnumerable<T>, so you can iterate over it. This operation 
                    /// is made thread-safe by making a snapshot of the collection when you start iterating it, so 
                    /// items added to the collection after you started iterating it won’t be visible. 
                    /// * That is why 21 will not be shown in most cases until the reading Thread is not freezed 
                    /// * for some time to give the time to the first task to add item before foreach statement will be called
                    foreach (int i in bag)
                        Console.WriteLine(i);
                }
                else
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | There is no output(the Reader's operation started earlier)");
                }
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task reading items from ConcurrentBag stops");
                task1.Wait();
            }).Wait();

        }

        /// <summary>
        /// ConcurrentStack has two important methods: Push and TryPop. Push is used to add an item 
        /// to the stack; TryPop tries to get an item off the stack. You can never be sure whether there are 
        /// items on the stack because multiple threads might be accessing your collection at the same 
        /// time.
        /// You can also add and remove multiple items at once by using PushRange and TryPopRange. 
        /// When you enumerate the collection, a snapshot is taken.
        /// </summary>
        void ConcurrentStack()
        {
            ConcurrentStack<int> stack = new ConcurrentStack<int>();
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Push(4);
            stack.PushRange(new int[] { 5, 6, 7, 8, 9, 10});

            Task task1 = Task.Run(() =>
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 starts.");
                    int[] range = new int[3];
                    stack.TryPopRange(range);
                    
                    Console.Write(DateTime.Now.ToLongTimeString() + " | Task 1 gets range of 3 values: ");
                    foreach(int i in range)
                        Console.Write(i+" ");
                    Console.WriteLine();

                    int value;
                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 gets " + value);
                    }
                    Thread.Sleep(99);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 gets " + value);
                    }
                    Thread.Sleep(99);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 gets " + value);
                    }
                    Thread.Sleep(99);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 gets " + value);
                    }
                    Thread.Sleep(99);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 gets " + value);
                    }


                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 ends.");
                });

            Task.Run(() =>
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 starts.");
                    int value;

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 gets " + value);
                    }
                    Thread.Sleep(100);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 gets " + value);
                    }
                    Thread.Sleep(100);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 gets " + value);
                    }
                    Thread.Sleep(100);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 gets " + value);
                    }
                    Thread.Sleep(100);

                    if (stack.TryPop(out value))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 gets " + value);
                    }

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 ends.");
                    task1.Wait();
                }).Wait();
        }

        /// <summary>
        /// ConcurrentQueue offers the methods Enqueue and TryDequeue to add and remove items 
        /// from the collection. It also has a TryPeek method and it implements IEnumerable by making a 
        /// snapshot of the data.
        /// </summary>
        void ConcurrentQueue()
        { 
            ConcurrentQueue<int> queue = new ConcurrentQueue<int>();
            queue.Enqueue(42);
            int result;
            if (queue.TryPeek(out result))
            {
                Console.WriteLine("Picked: {0}", result);
            }

            if (queue.TryDequeue(out result))
            {
                Console.WriteLine("Dequeued: {0}", result);
            }

            if (queue.TryPeek(out result))
            {
                Console.WriteLine("Picked: {0}", result);
            }
            else
            {
                Console.WriteLine("TryPeek returns false.");
            }
        }

        /// <summary>
        /// A ConcurentDictionary stores key and value in a thread-safe manner.
        /// 
        /// When working with a ConcurrentDictionary you have methods that can atomically add, 
        /// get, and update items. An atomic operation means that it will be started and finished as a 
        /// single step without other threads interfering. TryUpdate checks to see whether the current 
        /// value is equal to the existing value before updating it. AddOrUpdate makes sure an item is 
        /// added if it’s not there, and updated to a new value if it is. GetOrAdd gets the current value of 
        /// an item if it’s available; if not, it adds the new value by using a factory method.
        /// </summary>
        void ConcurentDictionary()
        {
            ConcurrentDictionary<string, int> dictionary = new ConcurrentDictionary<string, int>();
            int value;

            Task task1 = Task.Run(() =>
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 starts.");

                    if (dictionary.TryAdd("V1", 1))
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 adds V1 with value of 1");
                    }
                    else
                    {
                        Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 does not add V1 with value of 1");
                    }

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 is awaiting for 1 second");
                    Thread.Sleep(1000);

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 returns value of V1: " + dictionary["V1"]);

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 Add V1 with a value of 30 Or Update V1 with a value of 31");
                    dictionary.AddOrUpdate("V1", (key) => 30, (key, val) => 31);
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 is awaiting for 500 ms");
                    Thread.Sleep(500);

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 Trying Update V1 from 31 to 1");
                    dictionary.TryUpdate("V1", 1, 31);
                    Thread.Sleep(500);

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 returns result of TryRemove V1: " + dictionary.TryRemove("V1", out value) + " with a value of " + value);
                    Thread.Sleep(500);

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 Add V2 with a value of 100500 Or Update with a doubled value");
                    dictionary.AddOrUpdate("V2", 100500, (key, val) => val * 2);

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 ends.");
                });
            Task.Run(() =>
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 starts.");

                if (dictionary.TryAdd("V1", 1))
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 adds V1 with value of 1");
                }
                else
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 does not add V1 with value of 1");
                }

                if (dictionary.TryUpdate("V1", 2, 1))
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 updates V1 value to 2");
                }
                else
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is cannot update V1 value to 2");                   
                }

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 updates V1 value to 42 unconditionally");
                dictionary["V1"] = 42; // Overwrite unconditionally

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is awaiting for 1200 ms");
                Thread.Sleep(1200);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 returns value of V1: " + dictionary["V1"]);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is awaiting for 500 ms");
                Thread.Sleep(500);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 returns value of V1: " + dictionary["V1"]);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is awaiting for 500 ms");
                Thread.Sleep(500);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is trying to Get V1 and the result is: " + dictionary.TryGetValue("V1", out value) + " with value " + value);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is awaiting for 500 ms");
                Thread.Sleep(500);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is trying to Get V2 or Add value of 9, the result is: " + dictionary.GetOrAdd("V2", 9) );

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 returns result of TryRemove V2: " + dictionary.TryRemove("V2", out value) + " with a value of " + value);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 is trying to Get V2 or Add value of 9, the result is: " + dictionary.GetOrAdd("V2", 9));

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 ends.");
                task1.Wait();
            }).Wait();
        }
    }
}
