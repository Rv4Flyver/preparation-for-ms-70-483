﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Program_Flow
{
    class LockExample
    {
        /// <summary>
        /// It’s important to use the lock statement with a reference object that is private to the class. 
        /// A public object could be used by other threads to acquire a lock without your code knowing. 
        /// You should also avoid locking on the this variable because that variable could be used by 
        /// other code to create a lock, causing deadlocks.
        /// For the same reason, you should not lock on a string. Because of string-intering (the pro-
        /// cess in which the compiler creates one object for several strings that have the same content)
        /// you could suddenlu be asking for a lock on an onject that is used in multiple places.
        /// </summary>
        Object lockObject = new Object();
        int n;

        public LockExample()
        {
            n = 0;
            Task task = Task.Run(() =>
                {
                    for (int i = 0; i < 10; i++)
                    {
                        lock (lockObject)
                        {
                            Console.ResetColor();
                            Console.Write(" ");
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.Write(n);
                            n++;
                        }
                    }
                });

            for (int i = 0; i < 10; i++)
            {
                lock (lockObject)
                {
                    Console.ResetColor();
                    Console.Write(" ");
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write(n);
                    n--;
                }
            }

            task.Wait();
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" "+ n);

            /// The lock code is translated by the compiler into something that looks like
            object gate = new object();
            bool __lockTaken = false;
            try
            {
                Monitor.Enter(gate, ref __lockTaken);
            }
            finally
            {
                if (__lockTaken)
                    Monitor.Exit(gate);
            }
            /// You shouldn’t write this code by hand; let the compiler generate it for you. The compiler 
            /// takes care of tricky edge cases that can happen. 
        }
    }
}
