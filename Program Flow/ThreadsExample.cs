﻿using System;
using System.Threading;

namespace Program_Flow
{
    class ThreadsExample
    {
        #region Fields
        Thread thread_1;
        Thread thread_2;
        Thread thread_3;

        [ThreadStatic]
        static int thread_sleepTime = 0;                                                   // each thread gets its own copy of a thread_sleepTime field which 
                                                                                           // preserves default value until assigned new one in a particular thread
        ThreadLocal<string> endingMessage = new ThreadLocal<string>(() => "Threads are joined!");
        #endregion 

        public ThreadsExample()
        {
            #region Initialising threads
            // INITIALISING THREAD WITH A LAMBDA EXPRESSION
            thread_1 = new Thread(() =>
            {
                ThreadBody();
            });
            thread_1.Name = "Thread-1";

            
            /// INITIALISING THREAD WITH A DELEGATE ThreadStart
            /// thread_2 = new Thread(new ThreadStart(Thread2Method));                     // - Beginning in version 2.0 of the .NET Framework, it is not necessary to create a delegate explicitly.                                                                      
            thread_2 = new Thread(Thread2Method);                                          // - Specify the name of the method in the Thread constructor, and the compiler selects the correct delegate.
            thread_2.Name = "Thread-2";

            /// INITIALISING THREAD WITH A PARAMETERIZED DELEGATE ParameterizedThreadStart(Object)
            thread_3 = new Thread(new ParameterizedThreadStart(this.Thread3Method), 0);
            thread_3.Name = "Thread-3";
            thread_3.IsBackground = true;
            #endregion

            #region Starting threads
            thread_1.Start();
            thread_2.Start();
            thread_3.Start(5);                                                             // method executed in thread_3 accepts a parameter of Object type
            #endregion

            #region AwaitingThreads
            thread_1.Join();
            thread_2.Join();
            thread_3.Join();                                                               // without this statement main thread would wait for thread_3 and close an app immediately if no other foreground threads executing
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + endingMessage.Value);
            #endregion
        }

        /// <summary>
        /// The method for thread №2
        /// </summary>
        void Thread2Method()
        {
            ThreadBody();
        }

        /// <summary>
        /// The method for thread №3
        /// </summary>
        /// <param name="sec"></param>
        void Thread3Method(object sec)
        {
            thread_sleepTime = 5;
            endingMessage.Value =  Thread.CurrentThread.Name + " is finished";
            try
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + Thread.CurrentThread.Name + " is starting and will sleep for " + ((int)sec + thread_sleepTime) + " seconds");
                Thread.Sleep(thread_sleepTime*1000);
                for (int i = (int)sec; i > 0; i--)
                {
                    Console.Write("\r awaiting {0} seconds until exception will be thrown", i);
                    Thread.Sleep(1000);
                }
                thread_3.Abort();                                                          // throws a ThreadAbortException 
            }
            catch (ThreadAbortException)
            {
                Console.WriteLine("\r" + DateTime.Now.ToLongTimeString() + " | " + "Exception is thrown after awaiting " + ((int)sec + 5) + " seconds!");
                endingMessage.Value = Thread.CurrentThread.Name + " is aborted";
            }
            finally 
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + endingMessage);
            }
        }

        /// <summary>
        /// The method containing executable code for threads № 1 and 2 
        /// </summary>
        void ThreadBody()
        {
            endingMessage.Value = Thread.CurrentThread.Name + " is ending";
            thread_sleepTime = 3;
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + Thread.CurrentThread.Name + " is starting and will sleep for " + thread_sleepTime + " seconds");
            Thread.Sleep(thread_sleepTime*1000);
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + endingMessage.Value);
        }
    }
}
