﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Flow
{
    class InterlockedClassExample
    {
        public InterlockedClassExample()
        {
            IncrementDecrement();
            NonInterlockedCompareExchange();
            InterlockedCompareExchange();
        }

        /// <summary>
        /// Interlocked garantees that the increment and decrement operations are executed atomically.
        /// No other thread will see any intermediate results
        /// </summary>
        void IncrementDecrement()
        {
            Console.WriteLine("Interlocked Class Increment Decrement");
            int n = 0;
            var up = Task.Run(() =>
            {
                for (int i = 0; i < 1000000; i++)
                    Interlocked.Increment(ref n);
            });
            for (int i = 0; i < 1000000; i++)
                Interlocked.Decrement(ref n);
            up.Wait();
            Console.WriteLine(n);
        }

        /// <summary>
        /// This method first checks to see whether 
        /// the expected value is there; if it is, it replaces it with another value.
        /// </summary>
        void InterlockedCompareExchange()
        {
            Console.WriteLine("Compare Exchange");
            int value = 1;
            Task t1 = Task.Run(() =>
            {
                Interlocked.CompareExchange(ref value, 2, 1);
            });
            Task t2 = Task.Run(() =>
            {
                Interlocked.CompareExchange(ref value, 2, 1);
            });
            Task.WaitAll(t1, t2);
            Console.WriteLine(value);
        }

        void NonInterlockedCompareExchange()
        {
            Console.WriteLine("Witout Compare Exchange");
            int value = 1;
            Task t1 = Task.Run(() =>
            {
                if (value == 1)
                {
                    //Removing the following line will change the output
                    Thread.Sleep(1000);
                    value = 2;
                }
                Interlocked.CompareExchange(ref value, 2, 1);
            });
            Task t2 = Task.Run(() =>
            {
                value = 3;
            });
            Task.WaitAll(t1, t2);
            Console.WriteLine(value);
        }

    }
}
