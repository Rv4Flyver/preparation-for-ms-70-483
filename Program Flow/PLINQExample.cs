﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Program_Flow
{
    class PLINQExample
    {
        int upperBound = 10000000;
        IEnumerable<int> numbers;
        int[] result;
        DateTime startTime;

        public PLINQExample()
        {
            numbers = Enumerable.Range(0, upperBound);

            Console.WriteLine((startTime = DateTime.Now).ToLongTimeString() + " | " + "PLINQ query for even numbers from 0 to " + upperBound.ToString("0,0"));
            result = numbers.AsParallel().Where(i => i % 2 == 0).ToArray();
            PrintResult();

            Console.WriteLine((startTime = DateTime.Now).ToLongTimeString() + " | " + "LINQ query for even numbers 0 to " + upperBound.ToString("0,0"));
            result = numbers.Where(i => i % 2 == 0).ToArray();
            PrintResult();

            PrintProgressBar();
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "PLINQ query for showing even numbers from 0 to " + upperBound.ToString("0,0") + " starts.");
            PrintEvenNumbersAsParallel();
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "PLINQ query for showing even numbers from 0 to " + upperBound.ToString("0,0") + " ends.");
        }

        void PrintResult()
        {
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Elements count is " + result.Count().ToString("0,0") + ". Counted in " + (DateTime.Now - startTime).Milliseconds + "ms");       
        }

        /// <summary>
        /// In contrast to foreach, ForAll does not need all results before it starts executing. In this 
        /// example, ForAll does, however, remove any sort order that is specified.
        /// </summary>
        void PrintEvenNumbersAsParallel()
        {
            numbers.AsParallel().Where(i => i % 2 == 0).ForAll(i => Console.Write("\r" + DateTime.Now.ToLongTimeString() + " | " + i));
        }

        void PrintProgressBar()
        {
            for (int j = 80; j > 0; j--)
            {
                Thread.Sleep(100);
                Console.Write((char)22);
            }
        }
    }
}
