﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Flow
{
    class TasksCancellationExample
    {
        Task task, timeObserver;
        CancellationTokenSource cancellationTokenSource;
        CancellationToken token ;

        public TasksCancellationExample()
        {
            cancellationTokenSource = new CancellationTokenSource();
            /// The CancellationToken is used in the asynchronous Task. The CancellationTokenSource is 
            /// used to signal that the Task should cancel itself.
            token = cancellationTokenSource.Token;

            #region Tasks instantiation
            task = Task.Run(() => 
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task starts.");
                while(!token.IsCancellationRequested)   
                {
                    Thread.Sleep(1000);
                    if (!token.IsCancellationRequested) Console.Write("\r" + DateTime.Now.ToLongTimeString() + " | Task is working...");

                }
                Console.WriteLine();
                /// In this case, the operation will just end when cancellation is requested. Outside users of 
                /// the Task won’t see anything different because the Task will just have a RanToCompletion state. 
                /// If you want to signal to outside users that your task has been canceled, you can do this by 
                /// throwing an OperationCanceledException: 
                token.ThrowIfCancellationRequested();
            }, token);

            timeObserver = Task.Run(() =>
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Time Observer starts.");
                this.CancelTaskIn(10);
                Console.Write(DateTime.Now.ToLongTimeString() + " | Time Observer ends.");
            });
            #endregion

            try
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Awaiting 500 ms...");
                Thread.Sleep(500);

                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Press enter to stop awaiting");
                while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                cancellationTokenSource.Cancel();
            }
            catch (AggregateException e)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + e.InnerExceptions[0].Message);
            }

            #region ContinueWith OnCanceled
            /// You can also add a continuation Task that executes only when the Task is canceled.
            /// ?? In this Task, you have access to the exception that was thrown, and 
            /// ?? you can choose to handle it if that’s appropriate. 
            task.ContinueWith(t => 
            {
                // t.Exception.Handle((e) => true);
                Thread.Sleep(100);
                if (timeObserver.Status == TaskStatus.RanToCompletion) Console.WriteLine();
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Exception was arised manually.");
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Press enter to end the application");
            }, TaskContinuationOptions.OnlyOnCanceled);
            #endregion
        }
        /// <summary>
        /// If you want to cancel a Task after a certain amount of time, you also can use an overload of 
        /// Task.WaitAny that takes a timeout. Listing 1-45 shows an example.
        /// </summary>
        /// <param name="sec"></param>
        void CancelTaskIn(int sec)
        {
            int index = Task.WaitAny(new[] { task }, sec*1000);
            cancellationTokenSource.Cancel();
            if (index == -1)
                Console.WriteLine("\n"+DateTime.Now.ToLongTimeString() + " | " + "Task timed out and have been canceled");

        }

    }
}
