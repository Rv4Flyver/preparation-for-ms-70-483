﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Flow
{
    class AsyncAwaitExample
    {
        public AsyncAwaitExample()
        {
            Task task1 = new Task(() => 
                                {
                                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 starts");
                                    AsyncTaskMethod("Task 1"); 
                                    DisplayDigits("Task 1");
                                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 ends");
                                });
            task1.Start();

            Task task2 = Task.Run(async () => 
                                {
                                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 starts");
                                    await AsyncTaskMethod("Task 2").ConfigureAwait(false); 
                                    DisplayDigits("Task 2");
                                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 2 ends");
                                });
                                    
            task1.ContinueWith(task => { Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Task 1 continue its work"); DisplayDigits("Continuing Task 1"); Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Continuing Task 1 ends"); });
        }

        /// <summary>
        /// The async keyword marks a method for asynchronous operations. This way, 
        /// it signal to the compiler that something asynchronous is going to happen. The compiler 
        /// responds to this by transforming the code into a state machine. 
        /// 
        /// * The entry method of an application can’t be marked as async
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        async Task AsyncTaskMethod(string name)
        {
            /// A method marked with async just starts running synchronously on the current thread. 
            /// What it does is enable the method to be split into multiple pieces. The boundaries of these 
            /// pieces are marked with the await keyword.
            /// 
            /// A method marked with async just starts running synchronously on the current thread. 
            /// What it does is enable the method to be split into multiple pieces. The boundaries of these 
            /// pieces are marked with the await keyword.
            await Task.Delay(5000);
            /// E X A M  T I P
            /// When using async and await keep in mind that you should never have a method marked 
            /// async without any await statements. You should also avoid returning void from an async 
            /// method except when it’s an event handler.
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + name + " AsyncTaskMethod have been called with a 5 sec delay");
        }

        void DisplayDigits(string taskName)
        {
            for (int j = 1; j < 5; j++)
            {
                Thread.Sleep(1000);
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + taskName + " says " +j);
            }
        }
    }
}
