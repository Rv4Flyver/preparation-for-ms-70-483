﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Program_Flow
{
    /// <summary>
    /// The volatile keyword should be used to signal to the compiler that the 
    /// order of reads and writes on a field is important and that the compiler shouldn’t 
    /// change it.
    /// </summary>
    class VolatileExample
    {
        /// <summary>
        /// In C#, using the volatile modifier on a field guarantees that every access to 
        /// that field uses the Volatile.Read and Volatile.Write methods, but the volatile 
        /// modifier cannot be applied to array elements. The Volatile.Read and Volatile.
        /// Write methods can be used on array elements.
        /// </summary>
        volatile int volatile_flag;

        int _flag, _value;
        bool isSwitchedByCompiler;
        Task t1, t2;

        public VolatileExample()
        {

            Console.WindowHeight = 71;

            #region Non-Volatile Part
            isSwitchedByCompiler = false;
            while (!isSwitchedByCompiler)
            {
                Console.WriteLine("\n" + DateTime.Now.ToLongTimeString() + " | Main Thread resets values");
                _flag = _value = 0;
                Thread.Sleep(100);

                t1 = new Task(this.Thread1);
                t2 = new Task(this.Thread2);
                t1.Start();
                t2.Start();

                t2.Wait();
            }
            #endregion

            Console.WriteLine("\n" + DateTime.Now.ToLongTimeString() + " | Awaiting 5 sec");
            for (int j = 80; j > 0; j--)
            {
                Thread.Sleep(62);
                Console.Write((char)22);
            }

            #region Volatile Part
            int i = 0;
            while (i<10)
            {
                volatile_flag = _value = 0;
                Console.WriteLine("\n"+DateTime.Now.ToLongTimeString() + " | Main Thread resets values");
                Thread.Sleep(1000);

                t1 = new Task(this.Thread1_volatile);
                t2 = new Task(this.Thread2_volatile);
                t1.Start();
                t2.Start();

                i++;

                t2.Wait();
            }
            #endregion

        }

        #region Non-Volatile
        public void Thread1()
        {
            _value = 5;
            _flag = 1;
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Thread1 sets _value = 5 and _flag = 1");
        }
        public void Thread2()
        {
            if (_flag == 1)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | OK - Thread 2 says " + _value);
            }
            else
            {
                isSwitchedByCompiler = true;
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | ERROR - " + "Value was switched by a compiler - Thread 2 says " + _value);
            }
            t1.Wait();
        }
        #endregion

        #region Volatile
        public void Thread1_volatile()
        {
            _value = 5;
            volatile_flag = 1;
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Thread1_volatile sets _value = 5 and volatile_flag = 1");
        }

        public void Thread2_volatile()
        {
            if (volatile_flag == 1)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | OK - Thread2_volatile says " + _value);
            }
            else
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | ERROR - " + "Value was switched by a compiler: flag - " + volatile_flag + ", _value = "+_value);
            }
            t1.Wait();
        }
        #endregion
    }
}
