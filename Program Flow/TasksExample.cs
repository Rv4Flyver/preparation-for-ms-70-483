﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Flow
{
    /// <summary>
    /// Represents an asynchronous operation.
    /// </summary>
    class TasksExample
    {
        Task[] tasks = new Task[2];

        public TasksExample()
        {
            tasks[0] = new Task(() => { Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Task 0 begins and will print progress line in 1 second"); TaskMethod(80); tasks[1].Wait(); Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Task 0 ends"); });
            tasks[1] = new Task(StaticTaskMethod);

            tasks[0].Start();
            tasks[1].Start();

            tasks[0].ContinueWith(task => { Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Task 0 is continuing"); TaskMethod(80); Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Task 0 ends"); });

            Task.WaitAll(tasks);
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "All tasks have finished their work. Main thread will sleep for 2 sec");
            Thread.Sleep(2000);
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Awaiting for about 2 sec...");
            PrintingProgressBar(80);
        }

        void TaskMethod(Object sec)
        {
            PrintingProgressBar((int)sec);
        }

        void StaticTaskMethod()
        {
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Task 1 begins and will print progress line in 1 second");
            Thread.Sleep(1000);
            for (int i = 80; i > 0; i--)
            {
                Thread.Sleep(10);
                Console.Write((char)22);
            }
            Thread.Sleep(100);
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + "Task 1 awaits for 100ms and ends");
        }

        void PrintingProgressBar(int sec)
        {
            Thread.Sleep(1000);
            for (int i = (int)sec; i > 0; i--)
            {
                Thread.Sleep(10);
                Console.Write((char)22);
            }
        }
    }
}
