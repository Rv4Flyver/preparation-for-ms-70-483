﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Program_Flow
{
    /// <summary>
    /// Provides support for parallel loops and regions.
    /// </summary>
    class ParallelExample
    {
        public ParallelExample()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Parallel.For(1, 108, this.Method4ParallelExecution);
        }

        void Method4ParallelExecution(int sec)
        {
            lock(this) // wtf?!
            {
                if (sec <= 10 ) Console.BackgroundColor = ConsoleColor.Red;
                else if (sec <= 20) Console.BackgroundColor = ConsoleColor.Green;
                else if (sec <= 30) Console.BackgroundColor = ConsoleColor.Blue;
                else if (sec <= 40) Console.BackgroundColor = ConsoleColor.Yellow;
                else if (sec <= 50) Console.BackgroundColor = ConsoleColor.Gray;
                else if (sec <= 60) Console.BackgroundColor = ConsoleColor.Magenta;
                else if (sec <= 60) Console.BackgroundColor = ConsoleColor.DarkYellow;
                else if (sec <= 90) Console.BackgroundColor = ConsoleColor.Cyan;
                else if (sec <= 100) Console.BackgroundColor = ConsoleColor.White;
                else if (sec <= 110) Console.BackgroundColor = ConsoleColor.DarkCyan;
                Console.Write(sec + " ");
            }
        }

    }
}
