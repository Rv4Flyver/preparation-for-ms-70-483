﻿using System;
using System.Threading;

namespace Program_Flow
{
    /// <summary>
    /// Thread pooling is a form of multithreading in which tasks are added to a queue and automatically started when threads are created.
    /// Thread pools typically have a maximum number of threads. If all the threads are busy, additional tasks are placed in queue until they can be serviced as threads become available.
    /// Threads in thread pools by default are background
    /// </summary>
    class ThreadPoolExample
    {
        public ThreadPoolExample()
        {
            // SETTING MAX THREADS AND PRINTING STATISTICS
            // Sets the number of requests to the thread pool that can be active concurrently. All requests above that number remain queued until thread pool threads become available.
            if (ThreadPool.SetMaxThreads(1024, 1000))                           
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Worker threads was set to {0} and async I/O threads was set to {1}", 1024, 1000);
            }
            else
            { 
                // You cannot set the number of worker threads or the number of I/O completion threads to a number smaller than the number of processors in the computer.
                // If the common language runtime is hosted, for example by Internet Information Services (IIS) or SQL Server, the host can limit or prevent changes to the thread pool size.
            }
            PrintThreadPoolStatistics(0);

            // RUNNING THREADS
            ThreadPool.QueueUserWorkItem(this.Counting2MaxIntCallBack, 500);
            ThreadPool.QueueUserWorkItem(this.Counting2MaxIntCallBack, 0);
            ThreadPool.QueueUserWorkItem(this.Counting2MaxIntCallBack, 0);
            // WaitCallback is described as a delegate callback method to be called when the ThreadPool executes. It is a delegate that "calls back" its argument.
            ThreadPool.QueueUserWorkItem(new WaitCallback(PrintingProgressBar), 80);

            // PRINTING STATISTICS AFTER ALL THREADS HYPOTHETICALLY HAVE STARTED THEIR WORK
            PrintThreadPoolStatistics(5000);

            // PRINTING STATISTICS AFTER ALL THREADS HYPOTHETICALLY HAVE FINISHED THEIR WORK
            PrintThreadPoolStatistics(6000);
        }

        #region Static Methods
        static void PrintThreadPoolStatistics(int sleepTime)
        {
            Thread.Sleep(sleepTime);
            int workerThreads;
            int portThreads;
            ThreadPool.GetAvailableThreads(out workerThreads, out portThreads);
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | Avaliable {0} worker threads and {1} async I/O threads ", workerThreads, portThreads);
        }
        #endregion

        #region CallBack Methods
        void Counting2MaxIntCallBack(Object threadContext)
        {
            Thread.CurrentThread.Name = "Thread with start-value " + threadContext;
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + Thread.CurrentThread.Name + " started counting to " + Int32.MaxValue.ToString("0,0") + "...");
            for (int i = (int)threadContext; i < Int32.MaxValue; i++) ;
            Console.WriteLine(DateTime.Now.ToLongTimeString() + " | " + Thread.CurrentThread.Name + " finished counting to " + Int32.MaxValue.ToString("0,0") + "...");
        }

        void PrintingProgressBar(Object sec)
        {
            Thread.Sleep(1000);
            for (int i = (int)sec; i > 0; i--)
            {
                Thread.Sleep(10);
                Console.Write((char)22);
            }
        }
        #endregion
    }
}
