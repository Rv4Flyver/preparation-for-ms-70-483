﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Program_Flow
{
    class MainClass
    {
        static void Main(string[] args)
        {
            #region ExampleOptions
            List<string> options = new List<string>();
            options.Add("Threads");
            options.Add("Thread Pool");
            options.Add("Tasks");
            options.Add("Parallel Class");
            options.Add("Async and Await Example");
            options.Add("PLINQ Example");
            options.Add("Concurent Collections");
            options.Add("Lock");
            options.Add("Volatile Class");
            options.Add("The Interlocked class");
            #endregion

            #region Chose Option
            Console.WindowHeight = 80;
            Console.BufferWidth = Console.WindowWidth = 80;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Options: ");
            Console.ResetColor();
            {
                int i = 0;
                foreach(string option in options)
                {
                    Console.WriteLine(i+" "+option);
                    i++;
                }
                Console.WriteLine("? Summary");

                ConsoleKeyInfo chousenOption = Console.ReadKey(true);
                switch (chousenOption.KeyChar.ToString())
                {
                    case "0": 
                        { 
                            Console.Clear(); 
                            ThreadsExample(); 
                        } 
                        break;
                    case "1":
                        {
                            Console.Clear();
                            ThreadPoolExample();
                        }
                        break;
                    case "2":
                        {
                            Console.Clear();
                            TasksExample();
                        }
                        break;
                    case "3":
                        while (chousenOption.Key != ConsoleKey.Escape)
                        {
                            Console.Clear();
                            ParallelExample();
                            Console.ResetColor();
                            #region PrintProgressBar
                            for (int j = 80; j > 0; j--)
                            {
                                Thread.Sleep(100);
                                Console.Write((char)22);
                            }
                            #endregion
                        }
                        break;
                    case "4":
                        {
                            Console.Clear();
                            AsyncAwaitExample();
                        }
                        break;
                    case "5":
                        {
                            Console.Clear();
                            PLINQExample();
                        }
                        break;
                    case "6":
                        {
                            Console.Clear();
                            ConcurentCollectionsExample();
                        }
                        break;
                    case "7":
                        {
                            Console.Clear();
                            LockExample();
                        }
                        break;
                    case "8":
                        {
                            Console.Clear();
                            VolatileExample();
                        }
                        break;
                    case "9":
                        {
                            Console.Clear();
                            InterlockedClassExample();
                        }
                        break;
                    default:
                        {
                            Console.Clear();
                            ShowSummary();
                        }
                        break;
                }
            }
            #endregion

            Console.ReadKey();
        }

        static void ThreadsExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("T H R E A D S");
            Console.ResetColor();
            ThreadsExample threadsExample = new ThreadsExample();     
        }

        static void ThreadPoolExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("T H R E A D  P O O L");
            Console.ResetColor();
            ThreadPoolExample threadPoolExample = new ThreadPoolExample();
        }

        static void TasksExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("T A S K");

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Tasks Example");
            Console.ResetColor();
            TasksExample tasksExample = new TasksExample();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nTasks Cancellation Example");
            Console.ResetColor();
            TasksCancellationExample tasksCancellationExample = new TasksCancellationExample();
        }

        static void ParallelExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("P A R A L L E L  C L A S S");
            Console.ResetColor();
            ParallelExample parallelExample = new ParallelExample();
        }

        /// <summary>
        /// Doing a CPU-bound task is different from an I/O-bound task. CPU-bound tasks always 
        /// use some thread to execute their work. An asynchronous I/O-bound task doesn’t use a thread 
        /// until the I/O is finished.
        /// </summary>
        static void AsyncAwaitExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("A S Y N C  and  A W A I T");
            Console.ResetColor();
            AsyncAwaitExample parallelExample = new AsyncAwaitExample();    
        }

        static void PLINQExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("P L I N Q");
            Console.ResetColor();
            PLINQExample pLINQExample = new PLINQExample();
        }

        static void ConcurentCollectionsExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("C O N C U R E N T  C O L L E C T I O N S");
            Console.ResetColor();
            ConcurentCollectionsExample cce = new ConcurentCollectionsExample();
        }

        /// <summary>
        /// It’s important to synchronize access to shared data. One feature the C# language offers 
        /// is the lock operator, which is some syntactic sugar that the compiler translates in a call to 
        /// System.Thread.Monitor
        /// </summary>
        static void LockExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("L O C K");
            Console.ResetColor();
            LockExample lockExample = new LockExample();
        }

        /// <summary>
        /// The compiler sometimes changes the order of statements in your code. Normally, this 
        /// wouldn’t be a problem in a single-threaded environment. Volatile disables this optimization.
        /// </summary>
        static void VolatileExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("V O L A T I L E");
            Console.ResetColor();
            VolatileExample lockExample = new VolatileExample();       
        }

        /// <summary>
        /// Making operations atomic is the job of the Interlocked class that can be found in the System.Threading 
        /// namespace. When using the Interlocked.Increment or Interlocked.Decrement you create an atomic operation.
        /// </summary>
        static void InterlockedClassExample()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("I N T E R L O C K E D  C L A S S");
            Console.ResetColor();
            InterlockedClassExample ice = new InterlockedClassExample();
        }

        static void ShowSummary()
        {
            Console.WindowWidth = 120;
            Console.WriteLine("■ A thread can be seen as a virtualized CPU.");
            Console.WriteLine("\n■ Using multiple threads can improve responsiveness and enables you to make use of multiple processors.");
            Console.WriteLine("\n■ The Thread class can be used if you want to create your own threads explicitly.\n  Otherwise, you can use ThreadPool to queue work and let the runtime handle things.");
            Console.WriteLine("\n■ A Task object encapsulates a job that needs to be executed.\n  Tasks are the recommended way to create multithreaded code.");
            Console.WriteLine("\n■ The Parallel class can be used to run code in parallel.");
            Console.WriteLine("\n■ PLINQ is an extension to LINQ to run queries in parallel.");
            Console.WriteLine("\n■ The new async and await operators can be used to write asynchronous code more easily.");
            Console.WriteLine("\n■ Concurrent collections can be used to safely work with data in a multithreaded (current access) environment.");
            Console.WriteLine("\n________________________________________________________________________________________________________________________");
            Console.WriteLine("\n■ When accessing shared data in a multithreaded environment, you need to synchronize access to avoid errors or corrupted  data.");
            Console.WriteLine("\n■ Use the lock statement on a private object to synchronize access to a piece of code.");
            Console.WriteLine("\n■ You can use the Interlocked class to execute simple atomic operations.");
            Console.WriteLine("\n■ You can cancel tasks by using the CancellationTokenSource class with a CancellationToken.");
            Console.WriteLine("\n________________________________________________________________________________________________________________________");
            Console.WriteLine("\n■ Delegates are a type that defines a method signature and can contain a reference to a method.");
            Console.WriteLine("\n■ Delegates can be instantiated, passed around, and invoked.");
            Console.WriteLine("\n■ Lambda expressions, also known as anonymous methods, use the => operator and form a compact way of creating inline\n  methods.");
            Console.WriteLine("\n■ Events are a layer of syntactic sugar on top of delegates to easily implement the publish-subscribe pattern.");
            Console.WriteLine("\n■ Events can be raised only from the declaring class. Users of events can only remove and add methods the invocation\n  list.");
            Console.WriteLine("\n■ You can customize events by adding a custom event accessor and by directly using the underlying delegate type.");
            Console.ReadKey();
            Console.Clear();
            Main(new string[] {"0"});
        }
    }
}
