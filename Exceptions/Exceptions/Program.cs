﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                try
                {
                    Console.WriteLine("Inner try");
                    throw new Exception("Inner try exception");
                }
                catch (ArgumentNullException ex)
                {

                }
                finally
                {
                    Console.WriteLine("Inner finally");
                }
            }
            catch
            {
                Console.WriteLine("Outer catch");
            }
            finally
            {
                Console.WriteLine("Outer finally");
            }

            try
            {
                string str = null;
                PO(str);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadKey();
        }

        static void PO(string str)
        {
            if (str == null) throw new ArgumentNullException();
        }
    }
}
