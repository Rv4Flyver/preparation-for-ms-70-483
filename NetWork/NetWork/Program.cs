﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace NetWork
{
    class Program
    {
        static Stream dataStream;
        static StreamReader reader;
        static string URIAddress = "http://tauclan.tk/";

        static void Main(string[] args)
        {
            Console.SetWindowSize(200, 80);

            #region WebRequest
            Console.WriteLine("W E B  R E Q U E S T");
            try
            {
                WebRequest request = WebRequest.Create(URIAddress);
                request.UseDefaultCredentials = true;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Console.WriteLine(response.StatusDescription);

                dataStream = response.GetResponseStream();

                reader = new StreamReader(dataStream);

                Console.WriteLine(reader.ReadToEnd());

                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (WebException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            #endregion

            Console.ReadKey();

            ///The WebClient class provides common methods for sending data to or receiving data from any local, intranet, 
            ///or Internet resource identified by a URI.
            ///The WebClient class uses the WebRequest class to provide access to resources. WebClient instances can access 
            ///data with any WebRequest descendant registered with the WebRequest.RegisterPrefix method.
            #region WebClient

            Console.WriteLine("W E B  C L I E N T");
            WebClient client = new WebClient();
            /// A WebClient instance does not send optional HTTP headers by default. If your request requires an optional 
            /// header, you must add the header to the Headers collection. For example, to retain queries in the response, 
            /// you must add a user-agent header. Also, servers may return 500 (Internal Server Error) if the user agent 
            /// header is missing.
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

            dataStream = client.OpenRead(URIAddress);
            reader = new StreamReader(dataStream);
            Console.WriteLine(reader.ReadToEnd());

            /// You can use the CancelAsync method to cancel asynchronous operations that have not completed.

            reader.Close();
            dataStream.Close();

            #endregion

            Console.ReadKey();
        }

    }
}
