﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumeratorsComparors
{
    class IEnumerableCustomCollectionExample:IEnumerable, IEnumerator
    {
        string[] numbers;
        int index;

        public IEnumerableCustomCollectionExample(int from, int to)
        {
            if (from < to)
            {
                index = -1;
                numbers = new string[to - from];

                int n = 0;

                for (int i = from; i < to; i++)
                {
                    StringBuilder textNumber = new StringBuilder();
                    foreach (char ch in i.ToString())
                    {
                        switch (ch)
                        {
                            case '0': textNumber.Append("zero"); break;
                            case '1': textNumber.Append("one"); break;
                            case '2': textNumber.Append("two"); break;
                            case '3': textNumber.Append("three"); break;
                            case '4': textNumber.Append("four"); break;
                            case '5': textNumber.Append("five"); break;
                            case '6': textNumber.Append("six"); break;
                            case '7': textNumber.Append("seven"); break;
                            case '8': textNumber.Append("eight"); break;
                            case '9': textNumber.Append("nine"); break;
                        }
                        textNumber.Append(" ");
                    }
                    textNumber.AppendLine();
                    numbers[n]=textNumber.ToString();
                    n++;
                }
            }

        }

    
        public IEnumerator GetEnumerator()
        {
            return this;
        }

        public object Current
        {
            get { return numbers[index]; }
        }

        public bool MoveNext()
        {
            if (index == numbers.Length - 1)
            {
                Reset();
                return false;
            }

            index++;
            return true;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
