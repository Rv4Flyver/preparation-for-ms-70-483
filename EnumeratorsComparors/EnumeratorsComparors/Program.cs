﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EnumeratorsComparors
{
    delegate void DMethod(string str);

    class Program
    {
        static void Main(string[] args)
        {

            #region EnumeratorExample
            Console.WriteLine("Enumerator Example");
            IEnumeratorExample iee = new IEnumeratorExample(0, 15);
            iee.PrintUsingEnumerator();
            #endregion

            Console.WriteLine();

            #region DictionaryEnumeratorExample
            Console.WriteLine("Dictionary Enumerator Example");
            IDictionaryEnumeratorExample diee = new IDictionaryEnumeratorExample(0, 15);
            diee.PrintUsingEnumerator();
            #endregion

            Console.WriteLine();

            #region IEnumerableCustomCollectionExample
            Console.WriteLine("IEnumerable Custom Collection Example");
            IEnumerableCustomCollectionExample iecce = new IEnumerableCustomCollectionExample(0, 12);
            foreach (string str in iecce)
            {
                Console.WriteLine("\t" + str);
            }
            #endregion

            Console.WriteLine();

            #region YeldExample
            YeldExample ye = new YeldExample();
            #endregion

            Console.WriteLine();

            #region Enumerator Iterator example

            List<string> listString = new List<string> {"one", "two", "three", "four", "five", "six"};
            Console.WriteLine(GetOutput(listString.GetEnumerator())); 
            #endregion

            Expression<Func<string, int>> expr;
            expr = str => str.Length;
            int length = expr.Compile()("CS");
            Console.WriteLine(length);

            ReturnDelegate()("lol");

            Console.WriteLine();

            Console.ReadKey();
        }

        static string GetOutput(IEnumerator<string> iterator)
        {
            Func<int,char> suffix = col => col % 2 == 0 ? '\n' : '\t';
            StringBuilder output = new StringBuilder();
            for (int i = 1; iterator.MoveNext(); i++)
            {
                output.Append(iterator.Current);
                output.Append(suffix(i));
            }
                return output.ToString();
        }

        static DMethod ReturnDelegate()
        {
            DMethod d = DelMethod;
            return d;
        }

        static void DelMethod(string str)
        {
            Console.WriteLine("del - " + str);
        }
    }

 }
