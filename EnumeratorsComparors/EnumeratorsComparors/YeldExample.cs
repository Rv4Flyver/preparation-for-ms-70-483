﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumeratorsComparors
{
    class YeldExample
    {

        public YeldExample()
        {
            GalaxyClass.ShowGalaxies();
        }
    }

    public static class GalaxyClass
    {
        public static void ShowGalaxies()
        {
            var theGalaxies = new Galaxies();
            Console.WriteLine("IEnumerable");
            try
            {
                foreach (Galaxy theGalaxy in theGalaxies.NextGalaxy)
                {
                    Console.WriteLine(theGalaxy.Name + " " + theGalaxy.MegaLightYears);
                    throw new Exception();
                }
            }
            catch
            {

            }
            Console.WriteLine("\nIEnumerator");
            foreach (Galaxy gal in theGalaxies)
            {
                Console.WriteLine(gal.Name + " " + gal.MegaLightYears);
            }
        }

        public class Galaxies
        {
            //Error The body cannot be an iterator block because 'Galaxy' is not an iterator interface type.
            //public Galaxy GetEnumerator()   
            //{
            //    yield return new Galaxy { Name = "Tadpole", MegaLightYears = 400 };
            //    yield return new Galaxy { Name = "Pinwheel", MegaLightYears = 25 };
            //    yield return new Galaxy { Name = "Milky Way", MegaLightYears = 0 };
            //    yield return new Galaxy { Name = "Andromeda", MegaLightYears = 3 };
            //}

            public System.Collections.Generic.IEnumerator<Galaxy> GetEnumerator()
            {
                yield return new Galaxy { Name = "Tadpole", MegaLightYears = 400 };
                yield return new Galaxy { Name = "Pinwheel", MegaLightYears = 25 };
                yield return new Galaxy { Name = "Milky Way", MegaLightYears = 0 };
                yield return new Galaxy { Name = "Andromeda", MegaLightYears = 3 };
            }

            public System.Collections.Generic.IEnumerable<Galaxy> NextGalaxy
            {
                get
                {
                    try
                    {
                        yield return new Galaxy { Name = "Tadpole", MegaLightYears = 400 };
                        yield return new Galaxy { Name = "Pinwheel", MegaLightYears = 25 };
                        yield return new Galaxy { Name = "Milky Way", MegaLightYears = 0 };
                        yield return new Galaxy { Name = "Andromeda", MegaLightYears = 3 };
                    }
                    // if uncomment yield return in try-block will cause an error
                    //catch 
                    //{ }
                    finally
                    {
                        //Cannont be here
                        // yield return new Galaxy { Name = "Try-Finally", MegaLightYears = 0 };
                        // yield break;
                        Console.WriteLine("Executed as an exceprion in foreach block occures");
                    }

                    try
                    {
                        yield break;
                    }
                    catch
                    {
                        yield break;
                    }
                    finally
                    {
                        //Cannont be here
                        // yield return new Galaxy { Name = "Try-Finally", MegaLightYears = 0 };
                        // yield break;
                    }
                }
            }

        }

        public class Galaxy
        {
            public String Name { get; set; }
            public int MegaLightYears { get; set; }
        }
    }
}
