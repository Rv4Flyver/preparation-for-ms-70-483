﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumeratorsComparors
{
    class IEnumeratorExample
    {
        List<string> numbers;

        public IEnumeratorExample(int from, int to)
        {
            numbers = new List<string>();
            if (from < to)
            {
                for (int i = from; i <= to; i++)
                {
                    StringBuilder textNumber = new StringBuilder();
                    foreach (char ch in i.ToString())
                    {
                        switch (ch)
                        {
                            case '0': textNumber.Append("zero"); break;
                            case '1': textNumber.Append("one"); break;
                            case '2': textNumber.Append("two"); break;
                            case '3': textNumber.Append("three"); break;
                            case '4': textNumber.Append("four"); break;
                            case '5': textNumber.Append("five"); break;
                            case '6': textNumber.Append("six"); break;
                            case '7': textNumber.Append("seven"); break;
                            case '8': textNumber.Append("eight"); break;
                            case '9': textNumber.Append("nine"); break;
                        }
                        textNumber.Append(" ");
                    }
                    textNumber.AppendLine();
                    numbers.Add(textNumber.ToString());
                }
            }
        }

        public void PrintUsingEnumerator()
        {
            IEnumerator<string> enumer = numbers.GetEnumerator();
            while (enumer.MoveNext())
            {
                Console.Write("\t" + enumer.Current);
            }
            Console.WriteLine();
            enumer.Reset();
        }
    }
}
