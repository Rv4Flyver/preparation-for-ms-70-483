﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Validation_and_Conversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("number: {0,10:##-#}",123);

            ValidationAttributesExample vae = new ValidationAttributesExample();
            vae.Title = "123456789101112";
            Console.WriteLine(vae.Title);

            byte[] bytes = BitConverter.GetBytes(10);
            Console.WriteLine(BitConverter.ToString(bytes));
            Console.WriteLine(BitConverter.ToInt32(bytes,0));

            string dateStr;
            DateTime date, time;
            DateTimeOffset offsetDate;

            dateStr = "2000-02-02";
            //As time was not provided 12 AM is used
            time = DateTime.Parse(dateStr);
            Console.WriteLine("\tInput:  {0}", dateStr);
            Console.WriteLine("\tOutput: {0}\n", time);

            Console.WriteLine("DateTimeStyles examples:\n");

            Console.WriteLine(" -"+DateTimeStyles.NoCurrentDateDefault);
            dateStr = "12:00";
            date = DateTime.Parse(dateStr, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault);
            Console.WriteLine("\tInput:  {0}", dateStr);
            Console.WriteLine("\tOutput: {0}\n", date);

            Console.WriteLine(" -" + DateTimeStyles.AssumeLocal);
            dateStr = "Fri, 27 Feb 2009 03:11:21 +02:00";
            date = DateTime.Parse(dateStr, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal);
            Console.WriteLine("\tInput:  {0}", dateStr);
            Console.WriteLine("\tOutput: {0}\n", date);

            Console.WriteLine(" -" + DateTimeStyles.AdjustToUniversal);
            date = DateTime.Parse(dateStr, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            Console.WriteLine("\tInput:  {0}", dateStr);
            Console.WriteLine("\tOutput: {0}\n", date);

            Console.WriteLine(" -" + DateTimeStyles.AllowWhiteSpaces);
            dateStr = "    Fri,    27    Feb    2009     03:11:21     +02:00";
            date = DateTime.Parse(dateStr, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces);
            Console.WriteLine("\tInput string value:  {0}", dateStr);
            Console.WriteLine("\tOutput string value: {0}\n", date);

            // String with date and offset 
            dateStr = "05/01/2008 +1:00";
            offsetDate = DateTimeOffset.Parse(dateStr);
            Console.WriteLine(offsetDate.ToString());


            // Compares two Strings and returns –1, 0, or 1 to indicate that the first 
            // String should be considered before, equal to, or after the second 
            // String in the sort order. 
            Console.WriteLine(String.Compare("a", "b") + ", " + String.Compare("a", "a") + ", " + String.Compare("b", "a"));
            // -1, 0, 1

            // Overloaded versions of this method enable you 
            // to specify string comparison rules, whether to ignore case, and which 
            // culture’s comparison rules to use.
            Console.WriteLine(String.Compare("a", "B", true) + ", " + String.Compare("A", "0"));
            // -1, 1


            
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            string input = "1 2 3 4 5";
            string result = regex.Replace(input, " ");
            Console.WriteLine(result); // Displays 1 2 3 4 5 


            Lol<ValidationAttributesExample>(vae);

            Console.ReadKey();


        }

        static void Lol<T>(T lol) where T : ValidationAttributesExample
        {
            Console.WriteLine(lol);
        }
    }
}
